require 'multiple_table_inheritance/railtie'
require 'protected_attributes'

module MultipleTableInheritance
  extend ActiveSupport::Autoload

  autoload :Child
  autoload :Parent
  autoload :Migration
end
